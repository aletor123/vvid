def find_min_idx(array):
    min_idx = 0
    min_number = array[0]
    for i in range(len(array)):
        if min_number > array[i]:
            min_number = array[i]
            min_idx = i
    return min_idx


def find_elem_idx(array, elem):
    idx = 0
    for i in range(len(array)):
        if elem == array[i]:
            return i
    return -1


def find_lrg_seq(array):
    lrg_seq = 1
    cur_seq = 1
    for i in range(len(array) - 1):
        if array[i] == array[i + 1]:
            cur_seq += 1
        else:
            if cur_seq > lrg_seq:
                lrg_seq = cur_seq
            cur_seq = 1
    if cur_seq > lrg_seq:
        lrg_seq = cur_seq
    return lrg_seq


def find_lrg_elems(array):
    array.sort()
    return array[-3:]


def sort_arr(array):
    array.sort()
    return array


def compare_arrs(array, scnd_arr):
    if array == scnd_arr:
        return True
    else:
        return False