from unittest import TestCase

from arrayFunctions import find_min_idx, find_elem_idx, find_lrg_seq, find_lrg_elems, sort_arr, compare_arrs


class TestArrayFunctions(TestCase):
    def test_find_min_idx(self):
        self.assertEqual(find_min_idx([-30, 15, 25, 3, 4, -15, -70, 100]), 6)

    def test_find_elem_idx(self):
        self.assertEqual(find_elem_idx([-30, 15, 25, 3, 4, -15, -70, 100], -70), 6)

    def test_find_lrg_seq(self):
        self.assertEqual(find_lrg_seq([-30, 15, 15, 3, 4, -15, -15, -15, 2, 2, 2, 2, 2]), 5)

    def test_find_lrg_elems(self):
        self.assertEqual(find_lrg_elems([-30, 15, 25, 3, 4, -15, -70, 100]), [15, 25, 100])

    def test_sort_arr(self):
        self.assertEqual(sort_arr([-30, 15, 25, 3, 4, -15, -70, 100]), [-70, -30, -15, 3, 4, 15, 25, 100])

    def test_compare_arrs(self):
        self.assertEqual(compare_arrs([-30, 15, 25, 3, 4, -15, -70, 100], [-70, -30, -15, 3, 4, 15, 25, 100]), False)
        self.assertEqual(compare_arrs([-30, 15, 25, 3, 4, -15, -70, 100], [-30, 15, 25, 3, 4, -15, -70, 100]), True)

