from random import randint
import arrayFunctions

array = list()


def clear_array(func):
    def func_with_array_clearing():
        array.clear()
        func()

    return func_with_array_clearing


def is_number(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def get_int():
    number = input()
    while not is_number(number):
        print("введите число: ", end='')
        number = input()
    number = int(number)
    return number


def get_array_size():
    print("Введите размер массива: ", end='')
    size = get_int()
    return size


def input_array():
    size = get_array_size()
    arr = []
    for i in range(size):
        print("Введите {} элемент массива: ".format(i), end='')
        arr.append(get_int())
    return arr[:]


@clear_array
def hand_input():
    array.extend(input_array())


@clear_array
def auto_input():
    size = get_array_size()
    for i in range(size):
        array.append(randint(-100, 100))


def print_menu():
    print("1.Ввести массив вручную")
    print("2.Ввести массив автоматически")
    print("3.Вывести массив")
    print("4.Найти индекс минимального элемента массива")
    print("5.Найти индекс элемента массива")
    print("6.Отсортировать массив")
    print("7.Найти самую длинную последовательность элементов массива")
    print("8.Найти три наибольших элемента массива")
    print("9.Сравнить с массивом")


def menu():
    global array
    command = ''
    print_menu()
    while command != "exit":
        command = input("Введите команду: ")
        if command == "menu":
            print_menu()
        elif command == "1":
            hand_input()
        elif command == "2":
            auto_input()
        elif command == "3":
            print(array)
        elif command == "4":
            min_idx = arrayFunctions.find_min_idx(array)
            print("Индекс минимального элемента: {}".format(min_idx))
        elif command == "5":
            print("Введите значение элемента: ", end='')
            elem = get_int()
            elem_idx = arrayFunctions.find_elem_idx(array, elem)
            if elem_idx != -1:
                print("Индекс элемента: {}".format(elem_idx))
            else:
                print("Нет такого элемента в массиве")
        elif command == "6":
            array = arrayFunctions.sort_arr(array)
        elif command == "7":
            lrg_seq = arrayFunctions.find_lrg_seq(array)
            print("Длина максимальной последовательности: {}".format(lrg_seq))
        elif command == "8":
            lrg_elems = arrayFunctions.find_lrg_elems(array)
            print("Максимальные элементы: {}".format(lrg_elems))
        elif command == "9":
            scnd_arr = input_array()
            if arrayFunctions.compare_arrs(array,scnd_arr):
                print("Массивы равны")
            else:
                print("Массивы не равны")


if __name__ == '__main__':
    menu()
